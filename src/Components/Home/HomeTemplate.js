import React from "react";
import { useSelector } from "react-redux";
import LoadingComponent from "../Loading/LoadingComponent";
import FooterComponent from "./FooterComponent";
import HeaderComponent from "./HeaderComponent";

function HomeTemplate({ Component }) {
  const { loading } = useSelector((state) => state.ShipReducer);
  return (
    <>
      {loading ? (
        <LoadingComponent />
      ) : (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            height: "100vh",
          }}
        >
          <div
            style={{
              position: "fixed",
              width: "100%",
              zIndex: 9,
              background: "black",
              opacity: 0.7,
              height: "10vh",
            }}
          >
            <HeaderComponent />
          </div>
          <div style={{ paddingTop: 120, height: "80vh" }}>
            <Component />
          </div>
          {/* <div style={{
       background:'black',
       opacity:0.7,
       height:'10vh',
       color:'white',
       display:'flex',
       justifyContent:'center',
       alignItems:'center',
     }}>
       <FooterComponent />
     </div> */}
        </div>
      )}
    </>
  );
}

export default HomeTemplate;
