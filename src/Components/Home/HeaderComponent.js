import { Stack } from "@mui/material";
import React from "react";
import ButtonComponent from "../Button/ButtonComponent";

function HeaderComponent() {
  return (
    <div
      style={{
        height: "10vh",
      }}
    >
      <div style={{ height: "100%" }}>
        <Stack
          direction="row"
          spacing={2}
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            columnGap: 20,
            height: "100%",
          }}
        >
          <ButtonComponent url={"/"} name={"Home"} />
          <ButtonComponent url={"/about"} name={"About"} />
          <ButtonComponent url={"/contact"} name={"Contact"} />
          <ButtonComponent url={"/login"} name={"Login"} />
          <ButtonComponent url={"/test"} name={"Test"} />
        </Stack>
      </div>
    </div>
  );
}

export default HeaderComponent;
