import React from "react";
import { Helmet } from "react-helmet";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import useWindowSize from "../../Libs/Size/WindowSize";

function DetailComponent() {
  const size = useWindowSize();
  const {
    state: {
      ship: { name, type, image, missions, id, mmsi },
    },
  } = useLocation();
  const {ships} = useSelector(state=>state.ShipReducer);
  return (
    <>
      <div
        style={
          size?.width >= 700
            ? {
                display: "flex",
                columnGap: 10,
              }
            : {
                display: "flex",
                columnGap: 10,
                flexDirection: "column",
              }
        }
      >
        <Helmet>
          <title>Ship Detail - {id}</title>
          <meta name="description" content="Detail"></meta>
        </Helmet>
        <div
          style={
            size?.width >= 700
              ? { width: "50%", background: "#F5F5F5" }
              : {
                  display: "flex",
                  justifyContent: "center",
                  width: "100%",
                  background: "#F5F5F5",
                }
          }
        >
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              background: "#F5F5F5",
              height: "100%",
            }}
          >
            <img
              alt="123"
              src={image}
              width={300}
              height={400}
              style={{
                padding: 10,
                borderRadius: 15,
              }}
            />
          </div>
        </div>
        <div
          style={{
            width: "50%",
            background: "#F5F5F5",
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            padding: 10,
            rowGap: 20,
          }}
        >
          <label style={{ fontSize: 25 }}>
            ID: <label style={{ fontWeight: "bold" }}>{id}</label>
          </label>
          <label style={{ fontSize: 20 }}>
            Name: <label style={{ fontWeight: "bold" }}>{name}</label>
          </label>
          <label style={{ fontSize: 15 }}>
            Type: <label style={{ fontWeight: "bold" }}>{type}</label>
          </label>
          <label style={{ fontSize: 15 }}>
            MMSI: <label style={{ fontWeight: "bold" }}>{mmsi}</label>
          </label>

          <label style={{ fontWeight: "bold" }}>MISSIONS</label>

          <ul
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
              listStyleType: "none",
              height: "300px",
              overflow: "auto",
            }}
          >
            {missions.map((m, i) => {
              return (
                <li key={i}>
                  {m.flight} -{" "}
                  <label style={{ fontStyle: "italic", opacity: 0.5 }}>
                    {m.name}
                  </label>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </>
  );
}

export default DetailComponent;
