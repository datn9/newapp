import { Button } from "@mui/material";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { SET_PAGE } from "../../Redux/ConstType/ShipType";
import Content from "./Content";

function ContentComponent() {
  const {page} = useSelector(state=>state.ShipReducer);
  const dispatch = useDispatch();
  return (
    <>
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "space-evenly",
          rowGap: 10,
          columnGap: 10,
          padding: 20,
        }}
      >
        <Content />
        <nav
          style={{
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Button
          disabled={page>0 ? false : true}
          onClick={()=>{
            if(page>0){
              dispatch({
                type:SET_PAGE,
                page:page-1
              })
            }
          }}>Previous</Button>
          <span>{page + 1}</span>
          <Button onClick={()=>{
            dispatch({
              type:SET_PAGE,
              page:page+1
            })
          }}>Next</Button>
        </nav>
      </div>
    </>
  );
}

export default ContentComponent;
