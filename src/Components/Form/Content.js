import { useQuery } from "@apollo/client";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import useWindowSize from "../../Libs/Size/WindowSize";
import { GET_ALL_SHIPS_SCHEMAS} from "../../Schemas/ShipsSchemas";
import CardComponent from "../Card/CardComponent";
import LoadingComponent from "../Loading/LoadingComponent";
import {GET_ALL_SHIPS } from '../../Redux/ConstType/ShipType'
import { LIMIT_OFFSET_SHIPS } from "../../Schemas/LimitSchemas";

function Content() {
  const size = useWindowSize();
  const { ships,page,limit } = useSelector((state) => state.ShipReducer);
  const { data, loading,error } = useQuery(LIMIT_OFFSET_SHIPS,{
    variables:{
      limit:Number(limit),
      offset:page*Number(limit)
    }
  });
  const dispatch = useDispatch();
  useEffect(() => {
    if (!loading && data) {
      dispatch({
        type: GET_ALL_SHIPS,
        data: data?.ships,
      });
      console.log('set loading')
      dispatch({
        type:'SET_LOADING_DATA',
        loading:false
      })
    }
  }, [!loading && data]);
  if (error) return <pre>{error.message}</pre>
  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        flexWrap: "wrap",
        columnGap: 20,
        justifyContent: "space-evenly",
      }}
    >
       {ships?.length > 0 &&
          ships?.map((ship,i) => {
            return (
              <CardComponent key={i} size={size} ship={ship} />
            )
          })}
    </div>
  );
}

export default Content;
