import { useLazyQuery } from '@apollo/client';
import { Button, TextField } from '@mui/material';
import React, {useState } from 'react'
import { useDispatch } from 'react-redux';
import { FIND_SHIPS } from '../../Schemas/FindSchemas';

function FormComponent({type='text',label}) {
  const [search, setSearch] = useState("");
  const dispatch = useDispatch();
  const [executeSearch, { data, error, loading }] = useLazyQuery(FIND_SHIPS);

  console.log('data',data)
  const handleClick = () => {
    executeSearch({
      variables: { find: { id: search } },
    })
  };

  const handleChange = (e) => {
    console.log(e.target.value);
    setSearch(e.target.value);
  };
  return (
    <div style={{ display: "flex", justifyContent: "center" ,padding:10,marginBottom:10,columnGap:10}}>
        <TextField
          onChange={handleChange}
          type={type}
          id="standard-basic"
          label={label}
          variant="standard"
          value={search}
        />
        <Button
          onClick={handleClick}
          variant="contained"
          style={{background:'#F0EC8D', color:'black',opacity:0.7}}
        >
          {label}
        </Button>
      </div>
  )
}

export default FormComponent