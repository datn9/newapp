import { Button, Popover } from '@mui/material';
import React, { useState } from 'react'
import ShipLimit from '../Ships/ShipLimit';

function PopoverShipLimit() {

  const [anchorEl, setAnchorEl] = useState("");
  const handleClose = () => {
    setAnchorEl("");
  };
  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  return (
    <>
      <Button
        style={{ cursor: "pointer" }}
        aria-describedby={id}
        onClick={(event) => {
          setAnchorEl(event.currentTarget);
        }}
      >
        Limit
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <ShipLimit />
      </Popover>
    </>
  )
}

export default PopoverShipLimit