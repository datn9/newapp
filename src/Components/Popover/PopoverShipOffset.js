import { Button, Popover } from '@mui/material';
import React, { useState } from 'react'
import ShipLimit from '../Ships/ShipLimit';
import ShipOffset from '../Ships/ShipOffset';

function PopoverShipOffset() {

  const [anchorEl, setAnchorEl] = useState("");
  const handleClose = () => {
    setAnchorEl("");
  };
  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  return (
    <>
      <Button
        style={{ cursor: "pointer" }}
        aria-describedby={id}
        onClick={(event) => {
          setAnchorEl(event.currentTarget);
        }}
      >
        Offset
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <ShipOffset />
      </Popover>
    </>
  )
}

export default PopoverShipOffset