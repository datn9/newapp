import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button, CardActionArea, CardActions } from "@mui/material";
import Popover from "@mui/material/Popover";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Divider from "@mui/material/Divider";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import { useNavigate } from "react-router-dom";

function CardComponent({ship,size}) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const idPop = open ? "simple-popover" : undefined;
  const content = `${ship?.name} - ${ship?.type} ${ship?.mmsi ? `- ${ship?.mmsi}` : null}`

  const navigate = useNavigate();
  return (
    <div style={{paddingBottom:10,width:'20%'}}>
      <Card style={{boxShadow:'1px 1px 1px gray'}}>
        <CardActionArea onClick={()=>{
        navigate(`/detail/${ship?.id}`,{
          state:{ship}
        })
      }}>
          <CardMedia component="img" height="140" image={ship?.image} alt={ship?.name} />
          <CardContent>
            <Typography gutterBottom variant={size?.width>=1920 ? "h5" : size?.width >= 1024 ? "h6" : 'h7'} component="div">
              {ship?.id}
            </Typography>
            <Typography variant={size?.width >=1920 ? 'body2' : 'h7'} color="text.secondary" component={'label'} title={content} style={{cursor:'pointer'}}>
              {content?.length >= 27 ? content.substring(0,18)+'...' : content}
            </Typography> 
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button
            size="small"
            style={{background:'#F0EC8D', color:'black',opacity:0.7}}
            aria-describedby={ship?.Avatarid}
            variant="contained"
            onClick={handleClick}
          >
            Missions
          </Button>
          <Popover
            id={idPop}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left",
            }}
          >
            <Typography sx={{ p: 2 }} component={'span'}>
              {ship?.missions?.map((item, i) => {
                return (
                  <List
                    sx={{
                      width: "100%",
                      maxWidth: 360,
                      bgcolor: "background.paper",
                    }}
                    key={i}
                  >
                    <ListItem alignItems="flex-start">
                      <ListItemAvatar>
                        <Avatar alt="Travis Howard" src={ship?.image} />
                      </ListItemAvatar>
                      <ListItemText
                        primary={item?.flight}
                        secondary={ 
                          <React.Fragment>
                            <Typography
                              sx={{ display: "inline" }}
                              component="span"
                              variant="body2"
                              color="text.primary"
                            >
                              {item?.name}
                            </Typography>
                          </React.Fragment>
                        }
                      />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                  </List>
                );
              })}
            </Typography>
          </Popover>
        </CardActions>
      </Card>
    </div>
  );
}

export default CardComponent;
