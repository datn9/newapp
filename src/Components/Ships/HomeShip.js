import React  from "react";
import { Helmet } from "react-helmet";
import ContentComponent from "../Form/ContentComponent";
import ShipFind from "./ShipFind";
import ShipOrder from "./ShipOrder";

function HomeShip() {
  return (
    <div>
      <Helmet>
        <title>Home Ship</title>
        <meta name="description" content="Home"></meta>
      </Helmet>
      <ShipFind />
      <ShipOrder />
      <ContentComponent />
    </div>
  );
}

export default HomeShip;
