import React, { useEffect, useState } from "react";
import { NetworkStatus, useLazyQuery } from "@apollo/client";
import LoadingComponent from "../Loading/LoadingComponent";
import { LIMIT_OFFSET_SHIPS } from "../../Schemas/LimitSchemas";
import { useDispatch, useSelector } from "react-redux";
import { Button, TextField } from "@mui/material";

function ShipLimit() {
  const [limitNumber, setLimitNumber] = useState(1);
  const {offset,limit,page}= useSelector(state=>state.ShipReducer);
  const dispatch = useDispatch();
  const [executeLimit, { data, loading, error, networkStatus }] =
    useLazyQuery(LIMIT_OFFSET_SHIPS);
  useEffect(() => {
    if (!loading && data) {
      dispatch({
        type: "LIMIT_SHIPS",
        data: data?.ships,
      });
    }
  }, [!loading && data]);

  const handleChange = (e) => {
    setLimitNumber('limit NUmber',e.target.value);
    dispatch({
      type:'SET_LIMIT_NUMBER',
      limit:e.target.value
    })
  };

  const handleClick = () => {
    console.log("Limit number", limitNumber);
    executeLimit({
      variables: { limit: limit ? Number(limit) :0,offset:offset ? Number(offset) :0 },
    });
  };

  if (networkStatus === NetworkStatus.refetch) return "Refetching!";
  if (loading) return <LoadingComponent />;
  if (error) return <pre>{error.message}</pre>;
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        padding: 10,
        marginBottom: 10,
        columnGap: 10,
      }}
    >
      <TextField
        onChange={handleChange}
        type={"number"}
        id="standard-basic"
        label={"Limit"}
        variant="standard"
        value={limit ? limit >=0 ? limit :0 : 0}
      />
      <Button
        onClick={handleClick}
        variant="contained"
        style={{ background: "#F0EC8D", color: "black", opacity: 0.7 }}
      >
        {"Limit"}
      </Button>
    </div>
  );
}

export default ShipLimit;
