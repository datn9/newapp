import { useLazyQuery } from "@apollo/client";
import React, { useState } from "react";
import { Helmet } from "react-helmet";
import { ORDER_SHIPS } from "../../Schemas/OrderSchemas";
import ContentComponent from "../Form/ContentComponent";
import FormComponent from "../Form/FormComponent";
import LoadingComponent from "../Loading/LoadingComponent";

function ShipOrder() {
  const [orderData, setOrderData] = useState("");
  const [executeOrder, { data, error, loading }] = useLazyQuery(ORDER_SHIPS);

  if (loading) return <LoadingComponent />;
  if (error) return <p>{error.message}</p>;

  const handleClick = () => {
    console.log("Order number", orderData);
    executeOrder({
      variables: { order: orderData },
    });
    console.log("refetch");
  };

  const handleChange = (e) => {
    console.log(e.target.value);
    setOrderData(e.target.value);
  };

  return (
    <div>
      <Helmet>
        <title>Ship Order</title>
        <meta name="description" content="Order"></meta>
      </Helmet>
      <FormComponent
        handleClick={handleClick}
        handleChange={handleChange}
        label={"Order"}
        type={"text"}
        value={orderData}
      />
      <ContentComponent data={data} />
    </div>
  );
}

export default ShipOrder;
