import { useLazyQuery } from "@apollo/client";
import { Button, TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  SET_DISABLED,
  SET_LIMIT_NUMBER,
  SET_PAGE,
  SET_VISIBLE_PAGINATION,
} from "../../Redux/ConstType/ShipType";
import { FIND_SHIPS } from "../../Schemas/FindSchemas";
import LoadingComponent from "../Loading/LoadingComponent";

function ShipFind() {
  const [search, setSearch] = useState("");
  const [executeSearch, { data, error, loading }] = useLazyQuery(FIND_SHIPS);
  const dispatch = useDispatch();
  const { limit, page } = useSelector((state) => state.ShipReducer);
  useEffect(() => {
    if (!loading && data) {
      console.log("search length", data?.ships?.length, limit);
      dispatch({
        type: "FIND_SHIPS",
        data: data?.ships,
      });

      dispatch({
        type: SET_DISABLED,
        data: limit >= data?.ships?.length ? true : false,
      });
    }
  }, [!loading && data]);

  const handleClick = () => {
    dispatch({
      type: SET_PAGE,
      page: 0,
    });
    dispatch({
      type:SET_LIMIT_NUMBER,
      limit:0
    })
    executeSearch({
      variables: {
        find: { id: search },
        limit: Number(limit),
        offset: Number(page) * Number(limit),
      },
    });
    dispatch({
      type:SET_VISIBLE_PAGINATION,
      data:false
    })
  };

  const handleChange = (e) => {
    console.log(e.target.value);
    setSearch(e.target.value);
  };
  if (loading) return <LoadingComponent />;
  if (error) return <p>{error.message}</p>;
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        padding: 10,
        marginBottom: 10,
        columnGap: 10,
      }}
    >
      <TextField
        onChange={handleChange}
        type={"text"}
        id="standard-basic"
        label={"Search"}
        variant="standard"
        value={search}
      />
      <Button
        onClick={handleClick}
        variant="contained"
        style={{ background: "#F0EC8D", color: "black", opacity: 0.7 }}
      >
        {"Search"}
      </Button>
    </div>
  );
}

export default ShipFind;
