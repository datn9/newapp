import { useLazyQuery } from "@apollo/client";
import { Button, TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { LIMIT_OFFSET_SHIPS } from "../../Schemas/LimitSchemas";
import LoadingComponent from "../Loading/LoadingComponent";

function ShipOffset() {
  const [offsetNumber, setOffsetNumber] = useState(0);
  const dispatch = useDispatch();
  const {limit,offset,page} = useSelector(state=>state.ShipReducer);
  const [executeOffset, { data, error, loading }] = useLazyQuery(LIMIT_OFFSET_SHIPS);
  const handleClick = () => {
    console.log("Offset number", offsetNumber);
    executeOffset({
      variables: { offset: Number(offsetNumber),limit: limit ? Number(limit) :0 },
    });
    console.log("refetch");
  };

  const handleChange = (e) => {
    console.log(e.target.value);
    setOffsetNumber(e.target.value);
    dispatch({
      type:'SET_OFFSET_NUMBER',
      offset:e.target.value
    })
  };
  useEffect(() => {
    if(!loading && data){
      dispatch({
        type:'OFFSET_SHIPS',
        data:data?.ships
      })

      
    }
  }, [!loading && data])
  if (loading) return <LoadingComponent />;
  if (error) return <p>{error.message}</p>;
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        padding: 10,
        marginBottom: 10,
        columnGap: 10,
      }}
    >
      <TextField
        onChange={handleChange}
        type={"number"}
        id="standard-basic"
        label={"Offset"}
        variant="standard"
        value={offset ? offset >=1 ? offset :0 : 0}
      />
      <Button
        onClick={handleClick}
        variant="contained"
        style={{ background: "#F0EC8D", color: "black", opacity: 0.7 }}
      >
        {"Offset"}
      </Button>
    </div>
  );
}

export default ShipOffset;
