import { useLazyQuery } from "@apollo/client";
import {
  Button,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ORDER_SHIPS } from "../../Schemas/OrderSchemas";
import LoadingComponent from "../Loading/LoadingComponent";
import PopoverShipLimit from "../Popover/PopoverShipLimit";
import PopoverShipOffset from "../Popover/PopoverShipOffset";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import {
  ORDER_BY_CONDITION,
  SET_LIMIT_NUMBER,
  SET_PAGE,
  SET_VISIBLE_PAGINATION,
} from "../../Redux/ConstType/ShipType";
function ShipOrder() {
  const [condition, setCondition] = useState({
    sort: true,
    order: "name",
    name: { sort: true },
    type: { sort: true },
    image: { sort: true },
    mmsi: { sort: true },
    id: { sort: true },
  });
  const [executeOrder, { data, error, loading }] = useLazyQuery(ORDER_SHIPS);
  const dispatch = useDispatch();
  const { limit, page } = useSelector((state) => state.ShipReducer);
  useEffect(() => {
    if (!loading && data) {
      dispatch({
        type: ORDER_BY_CONDITION,
        data: data?.ships,
      });
      dispatch({
        type:SET_PAGE,
        page:0
      })
      dispatch({
        type:SET_LIMIT_NUMBER,
        limit:5
      })
      dispatch({
        type:SET_VISIBLE_PAGINATION,
        data:true
      })
    }
  }, [(!loading && data) || (limit || page)]);

  const handleClick = (order, sort) => {
    console.log("order&sort", order, sort);
    setCondition({ ...condition, sort, order, [order]: { sort } });
    executeOrder({
      variables: {
        order,
        sort: sort ? "asc" : "desc",
        limit: Number(limit),
        offset: page * limit,
      },
    });
  };
  if (loading) return <LoadingComponent />;
  if (error) return <p>{error.message}</p>;
  return (
    <div
      style={{
        display: "flex",
        flexWrap:'wrap',
        justifyContent: "center",
        columnGap: 10,
        padding: 10
      }}
    >
      <Button
        color={condition.order === "name" ? "secondary" : "primary"}
        style={{ cursor: "pointer" }}
        onClick={() => handleClick("name", !condition.name.sort)}
      >
        {condition.order === "name" ? (
          condition.name.sort ? (
            <ArrowUpwardIcon />
          ) : (
            <ArrowDownwardIcon />
          )
        ) : (
          <ArrowUpwardIcon />
        )}
        Name
      </Button>
      <Button
        color={condition.order === "type" ? "secondary" : "primary"}
        style={{ cursor: "pointer" }}
        onClick={() => handleClick("type", !condition.type.sort)}
      >
        {condition.order === "type" ? (
          condition.type.sort ? (
            <ArrowUpwardIcon />
          ) : (
            <ArrowDownwardIcon />
          )
        ) : (
          <ArrowUpwardIcon />
        )}
        Type
      </Button>
      <Button
        color={condition.order === "image" ? "secondary" : "primary"}
        style={{ cursor: "pointer" }}
        onClick={() => handleClick("image", !condition.image.sort)}
      >
        {condition.order === "image" ? (
          condition.image.sort ? (
            <ArrowUpwardIcon />
          ) : (
            <ArrowDownwardIcon />
          )
        ) : (
          <ArrowUpwardIcon />
        )}
        Image
      </Button>
      <Button
        color={condition.order === "id" ? "secondary" : "primary"}
        style={{ cursor: "pointer" }}
        onClick={() => handleClick("id", !condition.id.sort)}
      >
        {condition.order === "id" ? (
          condition.id.sort ? (
            <ArrowUpwardIcon />
          ) : (
            <ArrowDownwardIcon />
          )
        ) : (
          <ArrowUpwardIcon />
        )}
        Id
      </Button>
      <Button
        color={condition.order === "mmsi" ? "secondary" : "primary"}
        style={{ cursor: "pointer" }}
        onClick={() => handleClick("mmsi", !condition.mmsi.sort)}
      >
        {condition.order === "mmsi" ? (
          condition.mmsi.sort ? (
            <ArrowUpwardIcon />
          ) : (
            <ArrowDownwardIcon />
          )
        ) : (
          <ArrowUpwardIcon />
        )}
        MMSI
      </Button>
      <PopoverShipLimit />
      <PopoverShipOffset />
    </div>
  );
}

export default ShipOrder;
