import { useQuery } from "@apollo/client";
import { LinearProgress } from "@mui/material";
import React, { useEffect } from "react";
import { render } from "react-dom";
import InfiniteScroll from "react-infinite-scroll-component";
import { useDispatch, useSelector } from "react-redux";
import LoadDataReducer from "../../Redux/Reducer/LoadDataReducer";
import { GET_ALL_SHIPS_SCHEMAS } from "../../Schemas/ShipsSchemas";
import CardComponent from "../Card/CardComponent";
import LoadingComponent from "../Loading/LoadingComponent";

export default function ShipTest() {
  const dispatch = useDispatch();
  const { loading, error, data } = useQuery(GET_ALL_SHIPS_SCHEMAS);
  const {shipsArray,hasMore} = useSelector(state=>state.LoadDataReducer)

  useEffect(() => {
    if(!loading && data){
      dispatch({
        type:'GET_DATA_TEST',
        data:data?.ships
      })
    }
  }, [!loading && data])
  console.log('data',shipsArray)

  const fetchMoreData = () => {
   if(shipsArray?.length>=22){
    dispatch({
      type:'SET_LOADING',
      hasMore: false
    })
    return;
   }
    // a fake async api call like which sends
    // 20 more records in 1.5 secs
    setTimeout(() => {
      dispatch({
        type: "LOAD_DATA",
        data: data?.ships
      });
    }, 1500);
  };
  if(loading) return <LoadingComponent />
  return (
    <div>
      <h1>TEST INFINITE SCROLL COMPONENT</h1>
      <hr />
      <InfiniteScroll
        dataLength={shipsArray}
        next={fetchMoreData}
        hasMore={hasMore}
        loader={ <LinearProgress color="inherit" />}
        endMessage={
          <p style={{ textAlign: "center" }}>
            <b>NOT DATA</b>
          </p>
        }
      >
         {shipsArray?.map((ship, index) => (
          <div key={index}>
            <CardComponent ship={ship} />
          </div>
        ))}
      </InfiniteScroll>
    </div>
  );
}
