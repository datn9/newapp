import { CircularProgress, Stack } from "@mui/material";
import React from "react";

function LoadingComponent() {
  return (
    <div
      style={{
        display: "flex",
        height: "100vh",
        justifyContent: "center",
        alignItems: "center",
        background: "black",
        opacity: 0.1,
      }}
    >
      <Stack sx={{ color: "grey.500" }} spacing={2} direction="row">
        <CircularProgress color="inherit" />
      </Stack>
    </div>
  );
}

export default LoadingComponent; 