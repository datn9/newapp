import { Button } from "@mui/material";
import React from "react";
import { NavLink } from "react-router-dom";

const inActiveStyle= {
  textDecoration:'none',
  color:'white',
}

const activeStyle={
  ...inActiveStyle,
  color:'yellow',
  fontWeight:'bold'
}
function ButtonComponent({url,name}) {
  return (
    <Button>
      <NavLink  
        style={({isActive})=> isActive ? activeStyle : inActiveStyle}
        to={url}
      >
        {name}
      </NavLink>
    </Button>
  );
}

export default ButtonComponent;
