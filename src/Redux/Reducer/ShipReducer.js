import {
  FIND_SHIPS,
  GET_ALL_SHIPS,
  LIMIT_SHIPS,
  OFFSET_SHIPS,
  ORDER_BY_CONDITION,
  SET_DISABLED,
  SET_LIMIT_NUMBER,
  SET_OFFSET_NUMBER,
  SET_PAGE,
  SET_PAGINATION,
  SET_VISIBLE_PAGINATION,
} from "../ConstType/ShipType";

const initialState = {
  ships: [],
  page: 0,
  limit: 5,
  offset: null,
  disabled:false,
  visiblePagination:true,
  loading:false
};

export const ShipReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_SHIPS: {
      let ships = [...action.data];
      return { ...state, ships };
    }
    case FIND_SHIPS: {
      let ships = [...action.data];
      return { ...state, ships };
    }
    case LIMIT_SHIPS: {
      let ships = [...action.data];
      return { ...state, ships };
    }
    case OFFSET_SHIPS: {
      let ships = [...action.data];
      return { ...state, ships };
    }
    case ORDER_BY_CONDITION: {
      let ships = [...action.data];
      console.log("order by condition", ships);
      return { ...state, ships };
    }
    case SET_LIMIT_NUMBER: {
      let limit = action.limit;
      return { ...state, limit };
    }
    case SET_OFFSET_NUMBER: {
      let offset = action.offset;
      return { ...state, offset };
    }
    case SET_PAGE: {
      let page = action.page;
      return { ...state, page };
    }
    case SET_PAGINATION: {
      let ships = [...action.data];
      return { ...state, ships };
    }
    case SET_DISABLED:{
      let disabled = action.data;
      return {...state,disabled};
    }
    case SET_VISIBLE_PAGINATION:{
      let visiblePagination = action.data;
      return {...state,visiblePagination}
    }
    case 'LOAD_DATA':{
      let shipsArray = [...action.data];
      let ships = shipsArray.concat(Array.from({length:5}));
      console.log('reducer load data',ships)
      return {...state,shipsArray}
    }
    case 'SET_LOADING_DATA':{
      let loading = action.loading;
      console.log(loading,'reducer')
      return {...state,loading}
    }
    default: {
      return { ...state };
    }
  }
};
