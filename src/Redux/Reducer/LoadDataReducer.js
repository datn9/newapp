const initialState = {
  hasMore:true,
  shipsArray:[],
  newArray:[]
};

export const LoadDataReducer= (state = initialState, action) => {
  switch (action.type) {
    case 'GET_DATA_TEST':{
      let ships =[...action.data];
      let newArray = ships.slice(0,5);
      console.log('new Array',newArray)
      return {...state,shipsArray:newArray,newArray}
    }
    case 'LOAD_DATA':{
      let ships = [...action.data];
      let newArray =[...state.newArray];
      console.log('ships length',newArray?.length)
      let shipsArray = ships.slice(0,newArray?.length+5);
      console.log('reducer load data',shipsArray)
      newArray =[...shipsArray]
      return {...state,shipsArray,newArray}
    }
    case 'SET_LOADING':{
      let hasMore  =action.hasMore;
      console.log('has more',hasMore)
      return {...state,hasMore}
    }
    default: {
      return { ...state };
    }
  }
};
 