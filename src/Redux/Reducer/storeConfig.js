import { configureStore } from "@reduxjs/toolkit";
import { applyMiddleware, combineReducers } from "redux";
import {LoadDataReducer} from "./LoadDataReducer";
import { ShipReducer } from "./ShipReducer";

const rootReducer = combineReducers({
  ShipReducer,
  LoadDataReducer
})

export const store = configureStore({reducer:rootReducer})
