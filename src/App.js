import "./App.css";
import ShipLimit from "./Components/Ships/ShipLimit";
import ShipFind from "./Components/Ships/ShipFind";
import ShipOffset from "./Components/Ships/ShipOffset";
import ShipOrders from "./Components/Ships/ShipOrders";
import ShipOrder from "./Components/Ships/ShipOrder";
import { Navigate, Route, Routes } from "react-router-dom";
import HomeTemplate from "./Components/Home/HomeTemplate";
import DetailComponent from "./Components/Detail/DetailComponent";
import HomeShip from "./Components/Ships/HomeShip";
import ShipTest from "./Components/Ships/ShipTest";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<HomeTemplate Component={HomeShip} />} ></Route>
        <Route path="/detail/:id" element={<HomeTemplate Component={DetailComponent} />}></Route>
        <Route path="/test" element={<HomeTemplate Component={ShipTest} />}></Route>
        <Route path="*" element={<Navigate to="/" replace />} ></Route>
      </Routes>
    </div>
  );
}

export default App;
