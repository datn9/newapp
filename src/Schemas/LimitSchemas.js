import { gql } from "@apollo/client";

export const LIMIT_OFFSET_SHIPS = gql`
query limitShip($limit: Int!,$offset:Int!) {
  ships(limit: $limit, offset:$offset) {
    id
    image
    name  
    missions {
      flight
      name
    }
    type
    mmsi
  }
}
`;
