import { gql } from "@apollo/client";

export const GET_ALL_SHIPS_SCHEMAS = gql`
query getAllShips {
  ships {
    name
    type
    image
    id
    missions {
      name
      flight
    }
    mmsi
  }
}
`