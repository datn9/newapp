import { gql } from "@apollo/client";

export const SORT_SHIPS = gql`
query sortShips($sort:String!){
  ships(sort: $sort) {
    id
    image
    name
    missions {
      flight
      name
    }
    type
    mmsi
  }
}
`