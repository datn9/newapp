import { gql } from "@apollo/client";

export const ORDER_SHIPS = gql`
  query orderShips($order: String, $sort: String, $offset: Int, $limit: Int){
    ships(order: $order, sort: $sort, offset: $offset, limit: $limit) {
      id
      image
      name
      missions {
        flight
        name
      }
      type
      mmsi
    }
  }
`