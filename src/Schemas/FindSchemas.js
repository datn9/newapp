import { gql } from "@apollo/client";

export const FIND_SHIPS = gql`
  query findShips($find: ShipsFind, $offset: Int, $order: String) {
    ships(find: $find, offset: $offset, order: $order) {
      name
      type
      image
      id
      missions {
        name
        flight
      }
      mmsi
    }
  }
  
`